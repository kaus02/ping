import Ionic from 'ionic-scripts';
import { _ } from 'meteor/underscore';
import { Meteor } from 'meteor/meteor';
import { Controller } from 'angular-ecmascript/module-helpers';
import { Chats, Messages } from '../../../lib/collections';
import { MeteorCameraUI } from 'meteor/okland:camera-ui';
export default class ChatCtrl extends Controller {
    constructor() {
        super(...arguments);

        this.chatId = this.$stateParams.chatId;
        this.status;
        this.isIOS = Ionic.Platform.isWebView() && Ionic.Platform.isIOS();
        this.isCordova = Meteor.isCordova;

        this.helpers({
            messages() {
                return Messages.find({ chatId: this.chatId });
            },
            data() {
                return Chats.findOne(this.chatId);
            }
        });
        this.autoScroll();


    }




    sendPicture() {
        MeteorCameraUI.getPicture({}, (err, data) => {
            if (err) return this.handleError(err);

            this.callMethod('newPicture', {
                picture: data,
                //type: picture,
                chatId: this.chatId
            });
        });
    }
    sendMessage() {
        if (_.isEmpty(this.message)) return;

       var x= Chats.findOne({ _id: this.chatId});

        var otherId ;
       // console.log(x);
       // console.log(x.userIds[0]);
        //console.log(this.currentUser._id);
        if(x.userIds[0]===this.currentUser._id)
        {

            otherId=x.userIds[1];
        }
        else{

            otherId=x.userIds[0];
        }

        var otherUser = Meteor.users.findOne(otherId);
        //console.log(otherUser);
       if(otherUser.profile.flag===1)
        this.status=1;
        else
       {this.status=0;}
       //console.log(this.status);
        this.callMethod('newMessage', {
            text: this.message,
           // type: text,
            chatId: this.chatId,
            status: this.status
        });

        delete this.message;
    }

    autoScroll() {
        let recentMessagesNum = this.messages.length;

        this.autorun(() => {
            const currMessagesNum = this.getCollectionReactively('messages').length;
            const animate = recentMessagesNum != currMessagesNum;
            recentMessagesNum = currMessagesNum;
            this.scrollBottom(animate);
        });
    }

    inputUp () {
        if (this.isIOS) {
            this.keyboardHeight = 216;
        }

        this.scrollBottom(true);
    }

    inputDown () {
        if (this.isIOS) {
            this.keyboardHeight = 0;
        }

        this.$ionicScrollDelegate.$getByHandle('chatScroll').resize();
    }

    closeKeyboard () {
        if (this.isCordova) {
            cordova.plugins.Keyboard.close();
        }
    }

    scrollBottom(animate) {
        this.$timeout(() => {
            this.$ionicScrollDelegate.$getByHandle('chatScroll').scrollBottom(animate);
        }, 300);
    }
    handleError(err) {
        if (err.error == 'cancel') return;
        this.$log.error('Profile save error ', err);

        this.$ionicPopup.alert({
            title: err.reason || 'Save failed',
            template: 'Please try again',
            okType: 'button-positive button-clear'
        });
    }


    messageinfo(message) {
          this.$state.go('info',{id:message._id});
    }


    info(id) {
        //if (_.isEmpty()) return;

        var x= Messages.findOne({_id:id});
    var str;
 //   console.log(x);
           if(x.status===2)
               str = "Read";
            else
                str = "Delivered";
        const confirmPopup = this.$ionicPopup.confirm({
            title: 'Info',
            template: '<div>' + x.text + '</div><div>'+str+'</div>',
            cssClass: 'text-center',
            okText: 'Ok!',
            okType: 'button-positive button-clear'

        });

        confirmPopup.then((res) => {
            if (!res) return;

        });
    }



}

ChatCtrl.$inject = ['$stateParams', '$timeout', '$ionicScrollDelegate', '$ionicPopup', '$log', '$state'];