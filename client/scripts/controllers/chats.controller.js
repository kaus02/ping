import Moment from 'moment';
import { Controller } from 'angular-ecmascript/module-helpers';
import { Chats, Messages } from '../../../lib/collections';

export default class ChatsCtrl extends Controller {


    constructor() {
        super(...arguments);

        this.helpers({
            data() {
                return Chats.find();
            }
        });


    }
    showNewChatModal() {
        this.NewChat.showModal();
    }

    remove(chat) {
        this.callMethod('removeChat', chat._id);
    }

    read(id){

        this.read1(id);


    }
    read1(id){
        this.$interval(() => {
            var x = Messages.find({chatId: id});
            x.forEach((m) => {

                if (m.userId != this.currentUser._id  && m.status!=2) {

                    this.callMethod('updateMessage', m._id);


                }

               // console.log(m);

            });
    }, 100);

    }



}
ChatsCtrl.$inject = ['NewChat','$interval'];