import { Meteor } from 'meteor/meteor';
import { Chats, Messages } from '../lib/collections';

Meteor.methods({

    updateMessage(id){

        Messages.update(id,{$set:{status:2}});

    },

    newMessage(message) {
        if (!this.userId) {
            throw new Meteor.Error('not-logged-in',
                'Must be logged in to send message.');
        }
/*
        check(message(
            {
                text: String,
                //type: text,
                chatId: String
            }
        ));*/
        message.timestamp = new Date();
        message.userId = this.userId;
        message.type='text';


        const messageId = Messages.insert(message);
        Chats.update(message.chatId, { $set: { lastMessage: message } });
       // console.log(mesaage.status);

        //return messageId;
    },
    newPicture(message) {
        if (!this.userId) {
            throw new Meteor.Error('not-logged-in',
                'Must be logged in to send message.');
        }
        /*
        check(message(

            {
                picture: String,
               // type: picture,
                chatId: String
            }
        ));*/
        message.timestamp = new Date();
        message.userId = this.userId;
        message.type='picture';

        const messageId = Messages.insert(message);
        Chats.update(message.chatId, { $set: { lastMessage: message } });

        //return messageId;
    },
    updateName(name) {
        if (!this.userId) {
            throw new Meteor.Error('not-logged-in',
                'Must be logged in to update his name.');
        }

        check(name, String);

        if (name.length === 0) {
            throw Meteor.Error('name-required', 'Must provide a user name');
        }

        Meteor.users.update(this.userId, { $set:{ 'profile.flag': 1 } });
       // console.log(this.userId.flag);
        var s = Meteor.users.findOne(this.userId);
       // console.log(s);

        return Meteor.users.update(this.userId, {$set: {'profile.name': name}});
    },
    newChat(otherId) {
    if (!this.userId) {
        throw new Meteor.Error('not-logged-in',
            'Must be logged to create a chat.');
    }

    check(otherId, String);
    const otherUser = Meteor.users.findOne(otherId);

    if (!otherUser) {
        throw new Meteor.Error('user-not-exists',
            'Chat\'s user not exists');
    }

    const chat = {
        userIds: [this.userId, otherId],
        createdAt: new Date()
    };

    const chatId = Chats.insert(chat);

    return chatId;
},
    removeChat(chatId) {
        if (!this.userId) {
            throw new Meteor.Error('not-logged-in',
                'Must be logged to create a chat.');
        }

        check(chatId, String);

        const chat = Chats.findOne(chatId);

        if (!chat || !_.include(chat.userIds, this.userId)) {
            throw new Meteor.Error('chat-not-exists',
                'Chat not exists');
        }

        Messages.remove({ chatId: chatId });

        return Chats.remove({ _id: chatId });
    },
    updatePicture(data) {
        if (!this.userId) {
            throw new Meteor.Error('not-logged-in',
                'Must be logged in to update his picture.');
        }

        check(data, String);

        return Meteor.users.update(this.userId, {$set: {'profile.picture': data}});
    },

    out(){
          //  console.log(this.userId);
        Meteor.users.update(this.userId, { $set:{ 'profile.flag': 0 } });
        // console.log(this.userId.flag);
        var s = Meteor.users.findOne({_id:this.userId});
      //  console.log(s);

    }




});